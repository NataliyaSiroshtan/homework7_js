// DOM (Document Object Model) - это дерево объектов (объектное представление) - способ представить html теги в понятном для js виде. Другими словами еще, DOM - это представление html-документа в виде дерева тегов и каждый узел(тег) этого дерева - обьект. Обьектная модель документа позволяет получить доступ к содержимому html-документа, а также изменять содержимое, структуру и оформление этого документа.

let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function createList(newArray, parent = document.body) {
  const ul = document.createElement("ul");
  const elementList = newArray
    .map((element) => {
      return `<li>${element}</li>`;
    })
    .join("");
  ul.innerHTML = elementList;
  document.body.prepend(ul);
}
createList(array);
